
# Possibly rewrite the blitrotate that works on the Sprites instead of the images

########################################################################
# Worked on improving/cleaning up the code in anim-shapes-v2.py

# Umer Huzaifa
# UIUC RADLab
# March 26, 2019

from pygame_functions import *


# Importing the whole update function here to modify it for our purpose
#spriteGroup = pygame.sprite.OrderedUpdates()
#textboxGroup = pygame.sprite.OrderedUpdates()
# Background is a class with all the required parameters of color, image, size of screen
# Definition of the screen
# screen = pygame.display.set_mode([sizex, sizey], pygame.FULLSCREEN)
def updateDisplay():
    global background
    spriteRects = spriteGroup.draw(screen) # What does these two do here??
    textboxRects = textboxGroup.draw(screen)
    pygame.display.update()
    keys = pygame.key.get_pressed()
    if (keys[pygame.K_ESCAPE]):
        pygame.quit()
        sys.exit()
    spriteGroup.clear(screen, background.surface)
    textboxGroup.clear(screen, background.surface)

def blitRotate_Sprite(ds, sprite, pos, originPos, angle):

    image = sprite.image
    oldcenter = sprite.rect.center 
    # calcaulate the axis aligned bounding box of the rotated image
    w, h       = image.get_size()
    box        = [pygame.math.Vector2(p) for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
    box_rotate = [p.rotate(angle) for p in box]
    min_box    = (min(box_rotate, key=lambda p: p[0])[0], min(box_rotate, key=lambda p: p[1])[1])
    max_box    = (max(box_rotate, key=lambda p: p[0])[0], max(box_rotate, key=lambda p: p[1])[1])

    # calculate the translation of the pivot 
    pivot        = pygame.math.Vector2(originPos[0], -originPos[1])
    pivot_rotate = pivot.rotate(angle)
    pivot_move   = pivot_rotate - pivot

    # calculate the upper left origin of the rotated image
    origin = (pos[0] - originPos[0] + min_box[0] - pivot_move[0], pos[1] - originPos[1] - max_box[1] + pivot_move[1])    
    
    # get a rotated image
    rotated_image = pygame.transform.rotate(image, angle)
    change
    #
    w_rot, h_rot       = rotated_image.get_size()   
    rect_tuple = (origin[0],origin[1], w_rot, h_rot)
    # print(rect_tuple)
    updatedRect = pygame.draw.rect(ds, (0, 0, 1), rect_tuple)

    # rotate and blit the image
    ds.blit(rotated_image, origin)
    
    # Making the images sprites for conforming with pygame functions
    sprite.image = rotated_image    
    sprite.rect = updatedRect
    sprite.rect.center = oldcenter
    

screen = screenSize(750, 750)
x, y = (screen.get_width()/2, screen.get_height()/2 )
setBackgroundImage('background.png')

leg1 = makeSprite('leg1-resize.png')
moveSprite(leg1, x, y)
showSprite(leg1)
leg2 = makeSprite('leg2-resize.png')
moveSprite(leg2, x, y)
showSprite(leg2)
shaft = makeSprite('shaft-resize.png')
moveSprite(shaft, x, y)
showSprite(shaft)

angle   = 0
length = 0
img_top_x = 17
img_top_y = 216	
while True:	
	if keyPressed("up"): 
		angle+=5	
		length+=0.5
	elif keyPressed("down"):
		angle-=5
		length-=0.5
	transformSprite(leg1, angle, 1, hflip=False, vflip=False)	
	transformSprite(leg2, -angle, 1, hflip=False, vflip=False)	
	# blitRotate(screen, leg2.image, (x,y), (img_top_x, img_top_y), -angle)
	# blitRotate(screen, leg1.image, (x,y), (img_top_x, img_top_y), angle)	

	if length<0:
		transformSprite(shaft, 0, length, hflip=True, vflip=False)		
	# updateDisplay()
	# screen.fill([0,1,1])
	tick(30)