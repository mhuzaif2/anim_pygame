# Meant for figuring out a transform function for the sprites that can rotate them around a pivot on the images themselves

from pygame_functions import *

def updateDisplay():
    global background
    spriteRects = spriteGroup.draw(screen)
    pygame.display.update()
    keys = pygame.key.get_pressed()
    if (keys[pygame.K_ESCAPE]):
        pygame.quit()
        sys.exit()
    spriteGroup.clear(screen, background.surface)
    textboxGroup.clear(screen, background.surface)

def blitRotate(ds, image, pos, originPos, angle):
    
    # calcaulate the axis aligned bounding box of the rotated image
    w, h       = image.get_size()
    box        = [pygame.math.Vector2(p) for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
    box_rotate = [p.rotate(angle) for p in box]
    min_box    = (min(box_rotate, key=lambda p: p[0])[0], min(box_rotate, key=lambda p: p[1])[1])
    max_box    = (max(box_rotate, key=lambda p: p[0])[0], max(box_rotate, key=lambda p: p[1])[1])

    # calculate the translation of the pivot 
    pivot        = pygame.math.Vector2(originPos[0], -originPos[1])
    pivot_rotate = pivot.rotate(angle)
    pivot_move   = pivot_rotate - pivot

    # calculate the upper left origin of the rotated image
    origin = (pos[0] - originPos[0] + min_box[0] - pivot_move[0], pos[1] - originPos[1] - max_box[1] + pivot_move[1])    
    
    # get a rotated image
    rotated_image = pygame.transform.rotate(image, angle)    
    # rotate and blit the image
    ds.blit(rotated_image, origin)
    return rotated_image

def blitRotate_Sprite(ds, sprite, pos, originPos, angle):
    image = sprite.image
    oldcenter = sprite.rect.center
    oldwidth = sprite.rect.width
    oldheight = sprite.rect.height
    # oldrect = image.get_rect()
    # calcaulate the axis aligned bounding box of the rotated image
    w, h       = image.get_size()
    box        = [pygame.math.Vector2(p) for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
    box_rotate = [p.rotate(angle) for p in box]
    min_box    = (min(box_rotate, key=lambda p: p[0])[0], min(box_rotate, key=lambda p: p[1])[1])
    max_box    = (max(box_rotate, key=lambda p: p[0])[0], max(box_rotate, key=lambda p: p[1])[1])
    print(min_box)
    print(max_box)
    # calculate the translation of the pivot 
    pivot        = pygame.math.Vector2(originPos[0], -originPos[1])
    pivot_rotate = pivot.rotate(angle)
    pivot_move   = pivot_rotate - pivot

    # calculate the upper left origin of the rotated image
    origin = (pos[0] - originPos[0] + min_box[0] - pivot_move[0], pos[1] - originPos[1] - max_box[1] + pivot_move[1])    
    print(origin)
    # get a rotated image
    rotated_image = pygame.transform.rotate(image, angle)
    #
    w_rot, h_rot       = rotated_image.get_size()
    # print(w_rot)
    # print(h_rot)
    # print(origin)
    # rect_tuple = (origin[0],origin[1], w_rot, h_rot)
    # print(rect_tuple)

    updatedRect = pygame.draw.rect(ds, (0, 255, 255), (*origin, *rotated_image.get_size()),2)
    # print(updatedRect)
    # rotate and blit the image
    # ds.blit(rotated_image, origin)
    # ds.blit(rotated_image, updatedRect)
    # ds.blit(rotated_image, oldrect)
    # draw rectangle around the image
    # pygame.draw.rect (ds, (255, 0, 0), (*origin, *rotated_image.get_size()),2)

    # Making the images sprites for conforming with pygame functions
    # sprite.originalWidth = ro
    # sprite.image = rotated_image        
    # sprite.image = leg1_img    
    spriteRect = pygame.draw.rect(ds, (0, 0, 255), sprite.rect,2)
    sprite.rect = updatedRect
    # sprite.rect.center = oldcenter
    # sprite.originalWidth = oldwidth
    # sprite.originalHeight = oldheight
    showSprite(sprite)
    sprite.mask    = pygame.mask.from_surface(rotated_image)
    return sprite

##### Main Code Starting here ########
screen = screenSize(750, 750)
x, y = (screen.get_width()/2, screen.get_height()/2)

background = Background()
setBackgroundColour([0,255,0])
background.surface = screen.copy()

leg1 = makeSprite('leg1-resize.png')
leg1_img = loadImage('leg1-resize.png')
moveSprite(leg1,x - leg1.rect.width, y - leg1.rect.height)
showSprite(leg1)

ball = makeSprite('ball-resize.png')
moveSprite(ball, x, y - ball.rect.height/2)
showSprite(ball)
# print(ball.rect)


angle = 0
length = 0
img_top_x = 17
img_top_y = 216
shaft_col = [143, 142, 147]
while True:	
	if keyPressed('up'):
		angle+=5
		length+=0.5
	elif keyPressed('down'):
		angle-=5	
		length-=0.5	
    leg1 = blitRotate_Sprite(screen, leg1, (x,y), (img_top_x, img_top_y), -angle)   
    moveSprite(ball, x+length-15, y - ball.rect.height/2)
	
	# rot_img = blitRotate(screen, leg1_img, (x, y), (0,0), angle)
	# leg1.image = rot_img
	drawLine(x, y, x+length, y, shaft_col,4)
	
	# pygame.display.update()
	updateDisplay()	