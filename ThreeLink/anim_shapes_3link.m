% anim_shapes_3link.m

% Animating the biped by replacing lines with images 

% Umer Huzaifa
% RAD Lab UIUC
% 3rd Feb 2019

function anim_shapes_3link(options, Fstep)

    load('/Users/umerhuzaifa/Box Sync/Walker Project/3LinkStylGaitGen/MainCode/data.mat');
    x_final = x;
    nTime = size(x,2);
    
    switch nargin
        case 0
          options={'ColorScheme',2,'Disp',2,...
              'Lope'}; % Both legs in grey color,
                                              % Animate one step with shots
                                              % placed horizontally
          if (max(t)==1) 
              Fstep = 20;
          elseif(max(t)==2)
              Fstep = 10;
          end
                 
    end
    anim(t,x_final,Fstep,options)

function anim(t,x,Fs,options)
 
  [n,m]     = size(x');
  [vV,vH]   = hip_vel(x'); % convert angles to horizontal position of hips
  v         = [vH vV];
  pH_horiz  = zeros(n,1);
  pH        = zeros(n,2);

	% Estimate hip position by estimating integral of hip velocity
  for j=2:n       
        pH(j,:) ...
            = pH(j-1,:)+(t(j)-t(j-1))*v(j-1,:);
  end

  pH_horiz  = pH(:,1); % Take out the horizontal component of the hip position
  [te,pH_horiz] ...
            = even_sample(t',pH_horiz,Fs);
  [te,xe]   = even_sample(t',x',Fs);
  [n,m]     = size(xe);

  % Deciding the starting position for the feet and the hip
  offset    = [0;0];
  q         = x(1:3,1);
  [pFoot1,pFoot2,pH,pT] ...
            = limb_position(q, offset);

  cla        % Clear current axis.
  x_range   = [-1 5];
  y_range   = [0 4];
  anim_axis = [x_range y_range];
  axis(anim_axis)
  set(gca,'xtick',[])
  set(gca,'ycolor','None')
  hold on

  % Use actual relations between masses in animation
  [r,m,Mh,Mt,L,g] = model_params_three_link;
  scl             = 0.04; % factor to scale masses
  mr_legs         = m^(1/3)*scl; % radius of mass for legs
  mr_torso        = Mt^(1/3)*scl; % radius of mass for torso
  ground_color    = 'k'; % a.k.a. black

	% Approximate circular shape of mass
  param           = linspace(0,2*pi+2*pi/50,50);
  xmass_legs      = mr_legs*cos(param); % Definition of the circle
  ymass_legs      = mr_legs*sin(param); % representing legs COM
  xmass_torso     = mr_torso*cos(param); % Definition of the circle
  ymass_torso     = mr_torso*sin(param); % representing torso COM
    
  % Color Choices for Legs
  switch options{2}
        case 1
            leg1_color = [0.5,0.5,0.5];
            leg2_color = 0.2*[1,1,1];
            torso_color = 0.2*[1,1,1];
            hip_color = 0.2*[1,1,1];
        case 2
            leg1_color = [0,0,1];
            leg2_color = [1,0,0];
            torso_color = 'green';
            hip_color = 'green';
  end
  
  % Draw ground
  buffer = 5;
  ground = line([-buffer pH_horiz(n)+buffer],[0 0]);
  set(ground,'Color',ground_color,'LineWidth',2);

  % Draw leg one
  leg1   = line([pFoot1(1) pH(1)],[pFoot1(2) pH(2)]);
  
  %%% leg image size definition

  set(leg1,'LineWidth',2,'Color',leg1_color);

  % Draw leg two
  leg2   = line([pFoot2(1) pH(1)],[pFoot2(2) pH(2)]);

  set(leg2,'LineWidth',2,'Color',leg2_color);

  % Draw torso
  torso  = line([pH(1) pT(1)],[pH(2)*2 pT(2)*2]);
  torso_mass...
         = patch(xmass_torso+pT(1),ymass_torso+pT(2),torso_color);

  
  set(torso_mass,'EdgeColor',torso_color)
  set(torso,'LineWidth',2,'Color',torso_color);
  tot_step ...
         = 8; % total number of steps

  %%%% Video Recording Code
  vidObj = VideoWriter(options{5},'MPEG-4');  
  vidObj.FrameRate ...
         = Fs;
  vidObj.Quality ...
         = 100;
  open(vidObj);

  frameRate = vidObj.FrameRate;
  nFrame = floor(frameRate*t(end));
  frameDelay = 1/frameRate;  
  figHandle = gcf;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  nex_step = [0;0];
        for nstep = 1: tot_step
            for k=2:n
                offset = nex_step;
                q = xe(k,1:3);
                [pFoot1,pFoot2,pH,pT] = limb_position(q, offset);
                set(leg1,'XData',[pFoot1(1) pH(1)],'YData',[pFoot1(2) pH(2)]);
                set(leg2,'XData',[pFoot2(1) pH(1)],'YData',[pFoot2(2) pH(2)]);
                set(torso,'XData',[pH(1) pT(1)],...
                    'YData',[pH(2) pT(2)]);
                set(torso_mass,'XData',xmass_torso+pT(1), 'YData',ymass_torso+pT(2));

                drawnow;
                writeVideo(vidObj,getframe(figHandle))
                pause(frameDelay);                
            end
            nex_step = pFoot2;
            swap(leg1, leg2);
        end

  %%% --------------------------------------------------------------------------
  %%% a function to calculate hip velocity
  function [vV,vH] = hip_vel(x)

    [r,m,Mh,Mt,L,g]=model_params_three_link;
  	vV = zeros(length(x),1);
  	vH = r* cos(x(:,1)).*x(:,4); % estimate of horizontal velocity of hips
    vV = -r* sin(x(:,1)).*x(:,4);

  %%% --------------------------------------------------------------------------
  %%% a function to calculate the limb position
  function [pFoot1,pFoot2,pH,pT] = limb_position(q, offset)
    % Use position of hips as location of stance leg foot.
  	[r,m,Mh,Mt,L,g]=model_params_three_link;
  	pFoot1=[0; 0] + offset;
  	pH=[pFoot1(1)+r*sin(q(1)); pFoot1(2)+r*cos(q(1))];
  	pFoot2=[pH(1)-r*sin(q(2)); pH(2)-r*cos(q(2))];
  	pT=[pH(1)+q(3); pH(2)];



  %%% --------------------------------------------------------------------------
  %%% CONVERTS A RANDOMLY SAMPLED SIGNAL SET INTO AN EVENLY SAMPLED SIGNAL SET
  %%% (by interpolation)
  %%%
  %%% written by Haldun Komsuoglu, 7/23/1999
  function [Et, Ex] = even_sample(t, x, Fs)
  	% Obtain the process related parameters
  	N = size(x, 2);    % number of signals to be interpolated
  	M = size(t, 1);    % Number of samples provided
  	t0 = t(1,1);       % Initial time
  	tf = t(M,1);       % Final time
  	EM = (tf-t0)*Fs;   % Number of samples in the evenly sampled case with
  	% the specified sampling frequency
  	Et = linspace(t0, tf, EM)';
  	% Using linear interpolation (used to be cubic spline interpolation)
  	% and re-sample each signal to obtain the evenly sampled forms
  	for s = 1:N,
        Ex(:,s) = interp1(t(:,1), x(:,s), Et(:,1));
    end
