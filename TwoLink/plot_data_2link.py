import numpy as np
import pandas as dp
import matplotlib.pyplot as plt
from scipy.io import loadmat

anim_data = loadmat('data_lope.mat')

x = anim_data['x']
t = anim_data['t']
u = anim_data['u']
# F = anim_data['F']
param = anim_data['param']

q = x[:][0:2]
dq = x[:][2:4]

fig1 = plt.figure(1)
plt.subplot(121)
plt.plot(t.T, q[:][:].T)
plt.ylabel('Joint Positions (rad)')
plt.xlabel('Time (s)')
plt.title('Joint Positions Over a Walking Step')
plt.grid(True)


plt.subplot(122)
plt.plot(t.T, dq[:][:].T)
plt.ylabel('Joint Velocities (rad/s)')
plt.xlabel('Time (s)')
plt.title('Joint Velocities Over a Walking Step')
plt.grid(True)

plt.show()
